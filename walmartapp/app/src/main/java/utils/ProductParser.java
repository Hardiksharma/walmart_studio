package utils;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.walmartapp.Product;
import com.example.walmartapp.ProductController;

public class ProductParser {

	private static final String TAG = ProductParser.class.getSimpleName();

    public static final String KEY_ID = "productId";
    public static final String KEY_NAME = "productName";
    public static final String KEY_SHORTDESCRIPTION = "shortDescription";
    public static final String KEY_LONGDESCRIPTION = "longDescription";
    public static final String KEY_PRICE = "price";

    public static final String KEY_IMAGE = "productImage";
    public static final String KEY_REVIEWRATING = "reviewRating";

    public static final String KEY_REVIEWCOUNT = "reviewCount";
    public static final String KEY_INSTOCK = "inStock";
 
    /**
     * Parses the product and wraps it in product object
     * @param json
     * @return
     */
    public static void parseProduct(String json) throws JSONException{
        ProductLogger.i(TAG, "String returned from server: "+json);
        ProductController mPromController = ProductController.getInstance();
        Product product = null;
        JSONObject obj = new JSONObject(json);
        JSONArray list = obj.getJSONArray("products");
         if(list != null){
        	 for (int i =0; i< list.length(); i++){
        		 obj = list.getJSONObject(i);
        		 product = new Product();
                 product.setProductId(obj.optString(KEY_ID));
                 product.setName(obj.optString(KEY_NAME, ""));
                 product.setShortDesc(obj.optString(KEY_SHORTDESCRIPTION, ""));
                 product.setLongDesc(obj.optString(KEY_LONGDESCRIPTION, ""));
                 product.setPrice(obj.optString(KEY_PRICE, null));
                 product.setImageUrl(obj.optString(KEY_IMAGE, null));
                 product.setRating(obj.getInt(KEY_REVIEWRATING));
                 product.setReviewCount(obj.getInt(KEY_REVIEWCOUNT)); 
                 product.setInStock(obj.optBoolean(KEY_INSTOCK, false));
                 
                 mPromController.addProduct(product);
        	 }
        }
    }
}
