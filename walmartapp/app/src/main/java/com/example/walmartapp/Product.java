package com.example.walmartapp;

public class Product {

    private String mProductId;
    private String mPrice;
    private String mName;
    private int mReviewCount;
    private boolean mInStock;
    private String mShortDesc;
    private String mLongDesc;
    private String mImageUrl;
    private float mRating;

    public void setRating(float rating){
        this.mRating = rating;
    }
    public float getRating(){
    	return mRating;
    }
    public void setImageUrl(String imageUrl){
    	this.mImageUrl = imageUrl;
    }
    public String getImage(){
    	return mImageUrl;
    }
    public void setLongDesc(String longdesc){
    	this.mLongDesc = longdesc;
    }
    public String getLongDesc(){
    	return mLongDesc;
    }
    public void setShortDesc(String shortdesc){
    	this.mShortDesc = shortdesc;
    }
    public String getShortDesc(){
    	return mShortDesc;
    }
    public String getPrice(){
    	return mPrice;
    }
    public void setPrice(String price){
    	this.mPrice = price;
    }
    public String getProductId() {
       return mProductId;
    }
    public void setProductId(String id) {
       this.mProductId = id;
    }
    public int getReviewCount() {
        return mReviewCount;
    }
    public void setReviewCount(int reviewCount) {
        this.mReviewCount = reviewCount;
    }
    public String getName() {
        return mName;
    }
    public void setName(String name) {
        this.mName = name;
    }
    public boolean getInStock() {
        return mInStock;
    }
    public void setInStock(boolean inStock) {
        this.mInStock = inStock;
    }

    @Override
    public String toString() {
        return "Product [productId=" + mProductId + ", productName=" + mName
                + ", shortDescription=" + mShortDesc + ", longDescription=" + mLongDesc
                + ", price=" + mPrice + ", productImage=" + mImageUrl + ", reviewRating=" + mRating
                + ", reviewCount=" + mReviewCount + ", inStock=" + mInStock +"]";
    }
}
