package com.example.walmartapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

/**
 * Created by Hardik on 6/11/2015.
 */
public class SplashActivity extends Activity {

    private static final int SPLASH_DELAY = 2000;
    private Handler mHandler;
    private RelativeLayout mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mHandler = new Handler();
        mLayout = (RelativeLayout) findViewById(R.id.splash_layout);
        mLayout.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(this,
                R.anim.fadein_splash);
        mLayout.startAnimation(anim);
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent homeIntent = new Intent(getApplicationContext(),
                        MainActivity.class);
                startActivity(homeIntent);
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                finish();
            }

        }, SPLASH_DELAY);
    }
}

