package com.example.walmartapp;

import java.util.ArrayList;

import android.content.Context;

public class ProductController {

	private static final String TAG = ProductController.class.getSimpleName();

    private static ProductController mInstance = new ProductController();

    public ArrayList<Product> mProduct = new ArrayList<Product>();

    private ProductController() {
    }

    public static ProductController getInstance() {
        return mInstance;
    }
    
    public void clear() {
        mProduct.clear();
    }

    public void addProduct(Product product) {
        mProduct.add(product);
    }
}
