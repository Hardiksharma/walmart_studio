package com.example.walmartapp;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import utils.ProductLogger;

public class ProductHttpConnection {
	private final String TAG = ProductHttpConnection.class.getSimpleName();
	
	public String connect(String url) throws IOException
    {
         ProductLogger.i(TAG, "Connecting to the url: "+url);
         String returnStr = null;

         HttpClient httpclient = new DefaultHttpClient();
         HttpGet httpget = new HttpGet(url); 
         HttpResponse response;
         response = httpclient.execute(httpget);
         returnStr = EntityUtils.toString(response.getEntity());
         return returnStr;
    }

}
