package com.example.walmartapp;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Hardik on 6/10/2015.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ProductDetailFragment extends Fragment {

    public static ProductDetailFragment createFragmentForPosition(int position) {
        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        productDetailFragment.setArguments(bundle);
        return productDetailFragment;
    }

    private Product mDetailProduct;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        int position = arguments.getInt("position", 0);
        mDetailProduct = ProductController.getInstance().mProduct.get(position);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_detail_layout, container, false);

        TextView mTextName = (TextView) view.findViewById(R.id.detailname);
        RatingBar mRating = (RatingBar) view.findViewById(R.id.detailrating);
        TextView mReview = (TextView) view.findViewById(R.id.detailreview);
        ImageView mImage = (ImageView) view.findViewById(R.id.detailimage);
        TextView mPrice = (TextView) view.findViewById(R.id.detailprice);
        TextView mLongdesc = (TextView) view.findViewById(R.id.detaildescription);
        TextView mShortdesc = (TextView) view.findViewById(R.id.detailshortdescription);
        TextView mStockStatus = (TextView) view.findViewById(R.id.stockStatus);
        TextView mTextDescription = (TextView) view.findViewById(R.id.textDescription);

        mTextName.setText(mDetailProduct.getName());
        mRating.setRating(mDetailProduct.getRating());
        mReview.setText(Integer.toString(mDetailProduct.getReviewCount()));
        String imgUrl = mDetailProduct.getImage();
        Picasso.with(getActivity()).load(imgUrl).into(mImage);
        mPrice.setText(mDetailProduct.getPrice());
        mShortdesc.setText(Html.fromHtml(mDetailProduct.getShortDesc()));
        mLongdesc.setText(Html.fromHtml(mDetailProduct.getLongDesc()));

        if (mDetailProduct.getInStock()){
            mStockStatus.setText("In Stock");
        } else {
            mStockStatus.setText("Out of Stock");
        }

        if (TextUtils.isEmpty(mLongdesc.getText())){
            mTextDescription.setVisibility(View.GONE);
        }

        return view;
    }
}
