package com.example.walmartapp;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;

import utils.ProductLogger;
import utils.ProductParser;
import utils.Utils;


public class MainActivity extends Activity {

	private final ProductController mProductController = ProductController.getInstance();;
    private static final String EXTRA_CURRENT_PAGE = "EXTRA_CURRENT_PAGE";
    private ListView mListView;
    private ProductsAdapter mAdapter;
    private ProductDownloader mProductDownloader;
    private int mPreLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int currentPage;
        if (savedInstanceState == null) {
            currentPage = 0;
        } else {
            currentPage = savedInstanceState.getInt(EXTRA_CURRENT_PAGE, 0);
        }
        mProductDownloader = new ProductDownloader(this, currentPage);
        if (mProductController.mProduct.size() == 0) {
            mProductDownloader.nextPage();
        }

        mListView = (ListView) findViewById(R.id.listview);
        mAdapter = new ProductsAdapter(this, mProductController.mProduct);
        mListView.setAdapter(mAdapter);

        //OnScrolListener is for ListView and listens to when scroll reaches the last product
        // and basd on that nextpage method gets call to downoad next page of product in class ProjuctDownloader
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if (lastInScreen == totalItemCount) {
                    if(mPreLast !=lastInScreen){
                    //to avoid multiple calls for last item
                        mPreLast = lastInScreen;
                        mProductDownloader.nextPage();
                    }
                }
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                System.out.println("Item clicked" + position);
                Intent i = new Intent(MainActivity.this,ProductDetailActivity.class);
                // passing the position of the selected item to ProductDetailActivity
                i.putExtra("position", position);
                startActivity(i);
            }
        });
    }

    private void notifyFinish() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_CURRENT_PAGE, mProductDownloader.mCurrentPageNum);
    }

    //This ProductAdapter is extending BaseAdapter and helped in setting and getting all the views for all
    // the products.
    public class ProductsAdapter extends BaseAdapter {
    	  private final Context mContext;

          private final List<Product> mProducts;

    	  public ProductsAdapter(Context context, List<Product> values) {
    	    this.mContext = context;
            this.mProducts = values;
    	  }

        @Override
        public int getCount() {
            return mProducts.size();
        }

        @Override
        public Product getItem(int position) {
            return mProducts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
    	  public View getView(int position, View convertView, ViewGroup parent) {
    	    LayoutInflater inflater = (LayoutInflater) mContext
    	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	    final View rowView;
    	    if (convertView == null){
    	    	    rowView = inflater.inflate(R.layout.product_layout, parent, false);
    	    }else{
    	    	rowView = convertView;
    	    }
    	    final TextView textViewName = (TextView) rowView.findViewById(R.id.productname);
    	    final ImageView imageView = (ImageView) rowView.findViewById(R.id.productimage);
            final RatingBar textViewRating = (RatingBar) rowView.findViewById(R.id.productrating);
    	    final TextView textViewPrice = (TextView) rowView.findViewById(R.id.price);
            final TextView textViewReview = (TextView) rowView.findViewById(R.id.productreview);

    	    Product current = getItem(position);
    	    textViewName.setText(current.getName());
            String imgUrl = current.getImage();
            Picasso.with(mContext).load(imgUrl).into(imageView);
            textViewRating.setRating(current.getRating());
    	    textViewPrice.setText(current.getPrice());
            System.out.println("review count is" + current.getReviewCount());
            textViewReview.setText(Integer.toString(current.getReviewCount()));


    	    return rowView;
    	  }
    }

    // This class creates the basic url with key, page number, number of items and
    // fetch the data from the server.
    private class ProductDownloader {

        private final String TAG = ProductDownloader.class.getName();
        private final static String BASE_URL = "https://walmart-labs.appspot.com/_ah/api/walmart/v1/walmartproducts/";

        private final static String ID_1 = "4d851f0e-0dc5-11e5-a6c0-1697f925ec7b/";
        private int mCurrentPageNum;
        private final static String DEFAULT_PAGEITEMS = "/30";
        private Context mContext;

        public ProductDownloader(Context context, int currentPage) {
            mContext = context;
            mCurrentPageNum = currentPage;
        }

        // This method checks the network via isNetworkAvailable method in Util and calls Async task.
        private void tryAndFetch(){
            if (Utils.isNetworkAvailable(mContext)) {
                new HandleConnection().execute();
            } else {
                ProductLogger.i(TAG, "Internet connection not available");
            }
        }
        //This method gets call when nxt page is needed to be fetch
        public void nextPage(){
            mCurrentPageNum++;
            tryAndFetch();
        }

        /**
         * Async task which is responsible for connecting to the server
         * and downloading the data.
         *
         * @author
         */
        private class HandleConnection extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                //Reading the info - should save this info in DB
                try {
                    String pageUrl = BASE_URL + ID_1 + mCurrentPageNum + DEFAULT_PAGEITEMS;
                    String jsonStr = new ProductHttpConnection().connect(pageUrl);
                    ProductLogger.d(TAG, "doInBackground() /info " + jsonStr);
                    ProductParser.parseProduct(jsonStr);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                notifyFinish();
            }
        }
    }
}
